﻿using IO.Swagger.Api;
using IO.Swagger.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace client
{
    internal class Map
    {
        private readonly string name;
        private readonly int rewards;
        private readonly int tiles;
        private Node startNode = null;
        private Stack<Node> pathToExit = new Stack<Node>();
        private Stack<Node> pathToACollector = new Stack<Node>();

        public Map(string name, int rewards, int tiles)
        {
            this.name = name;
            this.rewards = rewards;
            this.tiles = tiles;
        }

        public string Name => name;

        public int Rewards => rewards;

        public int Tiles => tiles;
        private static int tags = 0;
        public void EnterMap(IO.Swagger.Api.MazesApi mazesApi, IO.Swagger.Api.MazeApi mazeApi)
        {
            IO.Swagger.Model.PossibleActionsAndCurrentScore currentTile = mazesApi.Enter(Name);
            startNode = new Node(mazeApi, currentTile);
            startNode.UpdateTag(++tags, true);
            startSearchingRecursive(mazeApi, startNode, out bool went);
            Console.WriteLine($"Just finished adding subnodes of map!");

        }


        private Node startSearchingRecursive(IO.Swagger.Api.MazeApi mazeApi, Node beginSearch, out bool finishedMap)
        {
            Node helperNode = beginSearch;
            finishedMap = false;
            if (helperNode.currentTile.PossibleMoveActions.All(a => (bool)a.HasBeenVisited) && helperNode.Previous != null)
            {
                try
                {
                    return goBack(mazeApi, out finishedMap, helperNode);
                }
                catch (InvalidOperationException)
                {

                }
            }
            if (helperNode.Previous == null && helperNode.currentTile.PossibleMoveActions.All(a => (bool)a.HasBeenVisited))
            {
                TryCollectMoney(mazeApi, helperNode.currentTile);
                finishedMap = true;
                return helperNode;
            }

            foreach (var action in helperNode.currentTile.PossibleMoveActions)
            {
                if (helperNode.currentTile.PossibleMoveActions.All(x => (bool)x.HasBeenVisited) && helperNode.Previous != null)
                {
                    helperNode = goBack(mazeApi, out finishedMap, helperNode);
                    if (helperNode.Previous==null && helperNode.currentTile.PossibleMoveActions.All(x => (bool)x.HasBeenVisited))
                    {
                        Console.WriteLine("Came back to starting point :D");
                        Console.WriteLine("Lets Search for an exit :)");
                        ExitRecursive(mazeApi, helperNode, out bool wentback);
                    }
                    continue;
                }
                if ((bool)action.HasBeenVisited || (helperNode.Previous != null && helperNode.Previous.Tag == action.TagOnTile))
                {
                    TryCollectMoney(mazeApi, helperNode.currentTile);
                    continue;
                }
                if (helperNode.Previous == null && helperNode.currentTile.PossibleMoveActions.All(x => (bool)x.HasBeenVisited))
                {
                    TryCollectMoney(mazeApi, helperNode.currentTile);
                    break;
                }

                var subNode = mazeApi.Move(action.Direction.Value.ToString());
                Console.WriteLine($"Moved {action.Direction.Value}, in hand: {subNode.CurrentScoreInHand}, in bag: {subNode.CurrentScoreInBag}");
                TryCollectMoney(mazeApi, subNode);
                Node newNode = new Node(mazeApi, subNode, helperNode);
                newNode.UpdateTag(++tags, true);
                var tempnode = startSearchingRecursive(mazeApi, newNode, out finishedMap);
                while (tempnode.currentTile.PossibleMoveActions.All(t => (bool)t.HasBeenVisited) && helperNode != tempnode)
                {
                    if (tempnode.Previous != null)
                    {
                        tempnode = goBack(mazeApi, out finishedMap, tempnode);
                    }
                    else
                    {
                        Console.WriteLine("Came back to starting point :D");
                        Console.WriteLine("Lets Search for an exit :)");
                        ExitRecursive(mazeApi, helperNode, out bool wentback);
                    }

                    if (tempnode.Tag == 1)
                    {
                        break;
                    }
                }
                helperNode = tempnode;

            }

            return helperNode;
        }

        private static Node goBack(MazeApi mazeApi, out bool finishedMap, Node helperNode)
        {
            TryCollectMoney(mazeApi, helperNode.currentTile);
            string direction;
            if (helperNode.currentTile.PossibleMoveActions.Count == 1)
            {
                direction = helperNode.currentTile.PossibleMoveActions.First().Direction.Value.ToString();

            }
            else
            {
                direction = helperNode.currentTile.PossibleMoveActions.Single(a => a.TagOnTile == helperNode.Previous.Tag).Direction.Value.ToString();
            }
            PossibleActionsAndCurrentScore possibleActionsAndCurrentScore = mazeApi.Move(direction);
            Console.WriteLine($"Moved {direction}, in hand: {possibleActionsAndCurrentScore.CurrentScoreInHand}, in bag: {possibleActionsAndCurrentScore.CurrentScoreInBag}");
            helperNode = helperNode.Previous;
            helperNode.currentTile = possibleActionsAndCurrentScore;
            finishedMap = true;
            return helperNode;
        }

        private static void TryCollectMoney(MazeApi mazeApi, PossibleActionsAndCurrentScore subNode)
        {
            try
            {
                if ((bool)subNode.CanCollectScoreHere && subNode.CurrentScoreInHand > 0)
                {
                    mazeApi.CollectScore();
                    Console.WriteLine($" --------------------------------------------------------------------------Just collected the money");
                }
            }
            catch (Exception e)
            {

            }
        }
        private static void TryCollectMoney(MazeApi mazeApi)
        {
            try
            {
                
                    mazeApi.CollectScore();
                    Console.WriteLine($" --------------------------------------------------------------------------Just collected the money");
                
            }
            catch (Exception e)
            {

            }
        }
        IDictionary<int, Node> alreadysearchednumber = new Dictionary<int,Node>();

        private static bool TryExit(MazeApi mazeApi)
        {
            try
            {
                mazeApi.ExitMaze();

                Console.WriteLine($"-------------------------------Just exited ---------------------------------------------------------------------------");
                return true;
            }
            catch (Exception ex)
            {
                return false;

            }
            return false;

        }

        private Node FindNextUnseenNode(List<int> listOfAllNode)
        {
            List<Node> vs = new List<Node>();
            vs = listOfAllNode.Where(k => alreadysearchednumber.ContainsKey(k)).Select(k=> alreadysearchednumber[k]).ToList();
            foreach (var i in vs)
            {
                if (!i.IsVisitedDuringSearch)
                {

                    return i;
                }
            }
            return null;
        }


        private Node ExitRecursive(IO.Swagger.Api.MazeApi mazeApi, Node beginSearch, out bool finishedMap)
        {
            Node helperNode = beginSearch;
            helperNode.IsVisitedDuringSearch = true;
            bool exited;
            helperNode.currentTile = mazeApi.PossibleActions();
            helperNode.currentTile.PossibleMoveActions.ForEach(move =>
            {
                if (!alreadysearchednumber.ContainsKey((int)move.TagOnTile))
                {
                    Node subMoveActionNude = new Node(mazeApi, move, helperNode);
                    subMoveActionNude.UpdateTag((int)move.TagOnTile, false);
                    alreadysearchednumber.Add((int)move.TagOnTile, subMoveActionNude);
                    if (!helperNode.SubNodesTags.Contains((int)move.TagOnTile)) { helperNode.SubNodesTags.Add((int)move.TagOnTile);}
                }
                else
                {
                    alreadysearchednumber[(int)move.TagOnTile].IsVisitedDuringSearch = true;
                }
            });
            TryCollectMoney(mazeApi);
            finishedMap = false;
            exited = TryExit(mazeApi);
            if (exited) { finishedMap = true; return helperNode; }
            bool checkIfVisited = helperNode.SubNodesTags.All(a => alreadysearchednumber.All(ab => (bool)ab.Value.IsVisitedDuringSearch && a == ab.Key));

            //if (helperNode.subNodesListForSearching.All(a => (bool)a.IsVisitedDuringSearch) && helperNode.Previous != null)
            if (checkIfVisited && helperNode.Previous != null)
                {
                try
                {
                    return goBack(mazeApi, out finishedMap, helperNode);
                }
                catch (InvalidOperationException)
                {

                }
            }

            if (helperNode.Previous == null && checkIfVisited)
            {

                TryCollectMoney(mazeApi);
                exited = TryExit(mazeApi);
                if (exited) { finishedMap = true; return helperNode; }
                return helperNode;
            }

            foreach (var action in helperNode.currentTile.PossibleMoveActions)
            {
                var nextMoveActionNode = FindNextUnseenNode(helperNode.SubNodesTags);
                if (checkIfVisited && nextMoveActionNode != null && action.TagOnTile == nextMoveActionNode.Tag && helperNode.Previous != null)
                {
                    helperNode = goBack(mazeApi, out finishedMap, helperNode);
                    continue;
                }
                if (checkIfVisited && helperNode.Previous != null)
                {
                    helperNode = goBack(mazeApi, out finishedMap, helperNode);
                    continue;
                }
                if ((bool)nextMoveActionNode.IsVisitedDuringSearch || (helperNode.Previous != null && helperNode.Previous.Tag == action.TagOnTile))
                {
                    TryCollectMoney(mazeApi);
                    exited = TryExit(mazeApi);
                    if (exited) { finishedMap = true; break; }
                    continue;
                }
                if (helperNode.Previous == null && checkIfVisited)
                {
                    TryCollectMoney(mazeApi);
                    exited = TryExit(mazeApi);
                    if (exited) { finishedMap = true; break; }
                    break;
                }

                var subNode = mazeApi.Move(nextMoveActionNode.moveAction.Direction.Value.ToString());
                Console.WriteLine($"Moved {nextMoveActionNode.moveAction.Direction.Value}, in hand: {subNode.CurrentScoreInHand}, in bag: {subNode.CurrentScoreInBag}");
                TryCollectMoney(mazeApi);
                exited = TryExit(mazeApi);
                if (exited) { finishedMap = true; break; }
                Node newNode = new Node(mazeApi, subNode, helperNode);
                newNode.UpdateTag(++tags, true);
                var tempnode = ExitRecursive(mazeApi, newNode, out finishedMap);
                if (finishedMap) break;

                while (tempnode.currentTile.PossibleMoveActions.All(t => (bool)t.HasBeenVisited) && helperNode != tempnode)
                {
                    if (tempnode.Previous != null)
                    {
                        tempnode = goBack(mazeApi, out finishedMap, tempnode);
                    }
                    else
                    {
                        break;
                    }

                    if (tempnode.Tag == 1)
                    {
                        break;
                    }
                }
                helperNode = tempnode;

            }

            return helperNode;
        }
    }
}