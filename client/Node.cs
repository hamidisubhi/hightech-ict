﻿using IO.Swagger.Api;
using IO.Swagger.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace client
{
    internal class Node
    {
        private int tag;
        private Node previous;
        private bool isVisitedDuringSearch;
        private readonly List<Node> subNodes = new List<Node>();
        private readonly List<Node> subNodesList = new List<Node>();
        private readonly List<int> subNodesTags = new List<int>();

        private MazeApi mazeApi;

        public Node(MazeApi mazeApi, PossibleActionsAndCurrentScore possibleActionsAndCurrentScore, Node previous = null)
        {
            currentTile = possibleActionsAndCurrentScore;
            this.previous = previous;
            this.mazeApi = mazeApi;
            addSubNode();
        }
        public Node(MazeApi mazeApi, MoveAction move, Node previous = null)
        {
            moveAction = move;
            this.previous = previous;
            this.mazeApi = mazeApi;
            addSubNode();
        }

        public PossibleActionsAndCurrentScore currentTile { get; set; }
        public MoveAction moveAction { get; set; }

        internal List<Node> SubNodes => subNodes;
        internal List<int> SubNodesTags => subNodesTags;
        internal List<Node> subNodesListForSearching => subNodesList;

        internal Node Previous { get => previous; set => previous = value; }

        public int Tag => tag;

        public void UpdateTag(int value, bool sendToAPI)
        {
            tag = value;
            if (sendToAPI)
            {
                mazeApi.Tag(value);
            }
            Console.WriteLine($"Just added node number {value}");
        }

        public bool IsVisitedDuringSearch { get => isVisitedDuringSearch; set => isVisitedDuringSearch = value; }
      
        private void addSubNode()
        {
            
        }
    }
}
