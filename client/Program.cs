﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IO;

namespace client
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            // Configure API key authorization
            IO.Swagger.Client.Configuration.Default.BasePath = "https://maze.hightechict.nl/";
            IO.Swagger.Client.Configuration.Default.AddDefaultHeader("Authorization", "HTI Thanks You [5ba8]");

            var playerApi = new IO.Swagger.Api.PlayerApi();
            //Console.WriteLine("About to register...");
            //try
            //{
            //    await playerApi.ForgetAsync();
            //}
            //catch (Exception)
            //{

            //}
            //await playerApi.RegisterAsync(name: "Subhi");

            var mazesApi = new IO.Swagger.Api.MazesApi();
            var mazeApi = new IO.Swagger.Api.MazeApi();

            Console.WriteLine("About to obtain all mazes...");
            var allMazes = await mazesApi.AllAsync();
            var allMaps = new List<Map>();
            foreach (var maze in allMazes)
            { 
                Console.WriteLine($"[{maze.Name}] has a potential reward of [{maze.PotentialReward}] and contains [{maze.TotalTiles}] tiles;");
                var map = new Map(maze.Name, (int)maze.PotentialReward, (int)maze.TotalTiles);
                allMaps.Add(map);
            }
            var i = 1;
            foreach (var map in allMaps)
            {
                if (map.Name == "PacMan") { 
                Console.WriteLine($"Entering Maze: {map.Name}");

                map.EnterMap(mazesApi, mazeApi);   }            
            }

            Console.ReadLine();
        }
    }
}
